"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_1 = require("./../../entity/user");
var user_service_1 = require('./../../services/user.service');
var UsersComponent = (function () {
    function UsersComponent(userService) {
        this.userService = userService;
        this.initUser();
    }
    UsersComponent.prototype.ngOnInit = function () {
        this.listUsers();
    };
    UsersComponent.prototype.listUsers = function () {
        var _this = this;
        this.userService
            .list()
            .then(function (users) { return _this.users = users; });
    };
    UsersComponent.prototype.initUser = function () {
        this.newUser = new user_1.User();
    };
    UsersComponent.prototype.create = function () {
        this.users.push(this.newUser);
        this.initUser();
    };
    UsersComponent.prototype.delete = function (index) {
        this.users.splice(index, 1);
    };
    UsersComponent = __decorate([
        core_1.Component({
            selector: 'users',
            templateUrl: './app/pages/user/user.component.html',
            styleUrls: ['./app/css/style.css'],
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService])
    ], UsersComponent);
    return UsersComponent;
}());
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=user.component.js.map