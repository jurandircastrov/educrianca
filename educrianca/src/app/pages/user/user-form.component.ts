import { Component, Input } from '@angular/core';
import { UserService } from './../../services/user.service';
import { User } from './../../entity/user';

@Component({ 
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./../../css/style.css']
})

export class UserFormComponent {

  constructor(private userService: UserService){

  }

    @Input()
    user: User;
   
    create(){
      this.user.score = 0;
      this.userService.insert(this.user);
    }
      
  
} 