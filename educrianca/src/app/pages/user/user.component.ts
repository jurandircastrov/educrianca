import { Component, OnInit } from '@angular/core';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { UserService } from './../../services/user.service';

import { User } from "./../../entity/user";


@Component({
	selector: 'users',
	templateUrl: './user.component.html',
	styleUrls: ['./../../css/style.css'],
})


export class UsersComponent implements OnInit {
	newUser: User;
	users: FirebaseListObservable<User[]>;


	constructor(private af: AngularFire, private userService: UserService){
		this.initUser();

		this.users = af.database.list('/users')
		this.users.subscribe(console.log)
	}

	initUser() {
        this.newUser = new User();
    }

	listUsers() {	
			this.userService
			.list() 
			this.users = this.af.database.list('/users');
	}

	ngOnInit() { 
        
				this.listUsers();
    }

	create(){
		this.userService
		.insert(this.newUser)

		this.users.push(this.newUser);
		this.initUser();
	}

	delete(key: string){
		this.users.remove(key);
	}

}