import { Component } from '@angular/core';
import { UserService } from './../../services/user.service';


@Component({
	selector: 'profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./../../css/style.css'],
})

export class ProfileComponent {

    name = UserService.currentUser.name;
    email = UserService.currentUser.email;
    age = UserService.currentUser.age;
    score = UserService.currentUser.score;

    constructor(){

        console.log(UserService.currentUser.name);
        
    }
    
    
}