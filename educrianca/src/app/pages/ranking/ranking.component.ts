import { Component} from '@angular/core';

import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { User } from './../../entity/user';
import { UserService } from './../../services/user.service';

@Component({ 
  selector: 'ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./../../css/style.css']
})

export class RankingComponent {

  users: FirebaseObjectObservable<User[]>;

  constructor( af: AngularFire, private userService: UserService) {
    this.users= af.database.object('/users');
    this.users.subscribe(console.log)
  }
  
  save(newName: string) {
    this.users.set({ name: newName });
  }
  update(newSize: string) {
    this.users.update({ size: newSize });
  }
  delete() {
    this.users.remove();
  }

} 