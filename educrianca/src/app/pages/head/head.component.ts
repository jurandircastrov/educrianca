import { Component, OnInit } from '@angular/core';
import { UserService } from './../../services/user.service';
import { User } from './../../entity/user';

@Component({

  selector: 'headLogin',
  templateUrl:  './head.component.html',
  styleUrls: ['./../../css/style.css']
})


export class HeadComponent implements OnInit { 

public email: string;
public password: string;
user: User;



constructor(private userService: UserService){

this.user = UserService.currentUser;


}

ngOnInit(): void{}

login(){
	this.userService.userLogin(this.email, this.password);
}

}
 
