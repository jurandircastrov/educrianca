import { Component} from '@angular/core';

import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { User } from './../../entity/user';

@Component({ 
  selector: 'users-list',
  templateUrl: './list.component.html',
  styleUrls: ['./../../css/style.css']
})

export class UsersListComponent {

  usersF: FirebaseObjectObservable<User[]>;
  constructor(af: AngularFire) {
    this.usersF = af.database.object('/user');
  }
  
  save(newName: string) {
    this.usersF.set({ name: newName });
  }
  update(newSize: string) {
    this.usersF.update({ size: newSize });
  }
  delete() {
    this.usersF.remove();
  }

} 