export class User{       
    public name : string;
    public password : string;
    public passwordConfirm : string;
    public email : string;
    public age  : number;
    public sex : string;
    public interest : string;
    public score : number;

}