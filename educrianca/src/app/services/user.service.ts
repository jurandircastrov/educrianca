import { Injectable } from '@angular/core';

import { AngularFire, FirebaseListObservable, AuthProviders, AuthMethods } from 'angularfire2'; 
import { Router } from '@angular/router';

import { User } from './../entity/user';

@Injectable()
export class UserService {
	users: FirebaseListObservable<User[]>;
	static currentUser: User;
	newUser : User;
	

	constructor(private af: AngularFire, private router:Router) {
		
		
	}

	getCUser(){
		return UserService.currentUser;
	}

	setCUser(user:User){

		UserService.currentUser = user;

	}
	
	userLogin(email: string, password: string): any{

			this.af.auth.login({email: email, password: password},
		{provider: AuthProviders.Password, method: AuthMethods.Password}).then(() =>
		{this.af.database.list('/users').subscribe((users) => {
			this.setCUser(users.filter(u => u.email === email)[0])
				this.router.navigate(['/profile']);


		})
	});
	}

	userLogout(){
		this.af.auth.logout();
		
	}

	list()  {
		this.users = this.af.database.list('/users');
	}


	insert(user: User) {
		this.users.push(user);
	    
		this.af.auth.createUser({email: user.email, password: user.password});

	}
}