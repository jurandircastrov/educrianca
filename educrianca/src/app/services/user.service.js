"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var UserService = (function () {
    function UserService() {
        this.users = [
            { name: 'Usuário 1', password: '123456', passwordConfirm: '123456', email: 'user1@gmail.com', age: 83, sex: 'masculino', interest: 'Lógica' },
            { name: 'Usuário 2', password: '123456', passwordConfirm: '123456', email: 'user2@gmail.com', age: 13, sex: 'feminino', interest: 'Lógica' },
            { name: 'Usuário 3', password: '123456', passwordConfirm: '123456', email: 'user3@gmail.com', age: 23, sex: 'masculino', interest: 'Lógica' },
        ];
    }
    UserService.prototype.list = function () {
        return Promise.resolve(this.users);
    };
    UserService.prototype.insert = function (user) {
        this.users.push(user);
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map