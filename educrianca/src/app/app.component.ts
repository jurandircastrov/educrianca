import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';


@Component({

  selector: 'my-app',
	styleUrls: ['./css/style.css'],
  template:  `
<headLogin></headLogin>

<div class="footer">
	<div class="container-fluid" id="container-rodape">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<!-- CREDITO DAS IMAGENS USADA NO SITE -->
					<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<a href="https://www.facebook.com/JuniorCastroV"><img src="app/img/facebook-logo.png">
					Developer facebook </a>
				</div>

					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<a href="http://www.faculdadenovaroma.com.br/"><img src="app/img/logo_fnr.png"> Faculdade Nova Roma</a>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				</div>

			</div>
		</div>
	</div>
</div>
<router-outlet></router-outlet>
`
})


export class AppComponent { 

}
 
