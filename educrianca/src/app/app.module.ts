import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';


import { AppComponent } from './app.component';

import { UserFormComponent } from './pages/user/user-form.component';
import { UsersComponent } from './pages/user/user.component';
import { UsersListComponent } from './pages/listUsers/list.component';
import { IndexComponent } from './pages/index/index.component';
import { MenuLateralComponent } from './pages/menuLateral/menuLateral.component';
import { GamesListComponent } from './pages/listGames/listGames.component';
import { UserService } from './services/user.service';
import { GameCognicaoComponent } from './pages/games/gameCognicao/gameCognicao.component';
import { GameMatematicaComponent } from './pages/games/gameMatematica/gameMatematica.component';
import { HeadComponent } from './pages/head/head.component';
import { RankingComponent } from './pages/ranking/ranking.component';
import { ProfileComponent } from './pages/profile/profile.component';


const myFirebaseConfig = {
  apiKey: 'AIzaSyCTDjDHJb7GExQbLZKoSwi_tVjANGPNyKk',
  authDomain: 'edrucrianca.firebaseapp.com',
  databaseURL: 'https://edrucrianca.firebaseio.com',
  storageBucket: 'edrucrianca.appspot.com',
  messagingSenderId: "208991820744"
}

@NgModule({

 declarations: [
    AppComponent, 
    GameCognicaoComponent, 
    UsersComponent, 
    UserFormComponent, 
    UsersListComponent, 
    IndexComponent,
    GamesListComponent,
    MenuLateralComponent,
    HeadComponent,
    ProfileComponent,
    GameMatematicaComponent,
    RankingComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(myFirebaseConfig),
    RouterModule.forRoot([
          {
            path: 'index',
            component: IndexComponent
          },
          {
            path: 'gameCognicao',
            component: GameCognicaoComponent
          },
           {
            path: 'gameMatematica',
            component: GameMatematicaComponent
          },
          {
            path: 'games-list',
            component: GamesListComponent
          },
           {
            path: 'users',
            component: UsersComponent
          },
          {
            path: 'ranking',
            component: RankingComponent
          },
          {
            path: 'profile',
            component: ProfileComponent
          },
          {
            path: '',
            redirectTo: '/index',
            pathMatch: 'full'
          }
        ])

  ],

  providers: [ UserService ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
